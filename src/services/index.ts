//#region IMPORT
// Libraries
import axios, {AxiosResponse} from 'axios';
// Utils
import config from 'config/env';
// Data
import {Coordinates} from './entity';
//#endregion

const client = axios.create(config.api);

const OpenCageServices = {
  getCoordinatesOfAddress(address: string): Promise<AxiosResponse<any, any>> {
    return client.request({
      method: 'get',
      url: `${config.api.openCage}/geocode/v1/json?`,
      params: {
        key: config.appKey.openCage,
        q: address,
        language: 'en',
      },
    });
  },
  getAddressOfCoordinates(
    lat: number,
    lon: number,
  ): Promise<AxiosResponse<any, any>> {
    return client.request({
      method: 'get',
      url: `${config.api.openCage}/geocode/v1/json?`,
      params: {
        key: config.appKey.openCage,
        q: `${lat}+${lon}`,
        language: 'en',
      },
    });
  },
};

const OpenWeatherServices = {
  getWeatherAndForecast(
    coordinates: Coordinates | undefined,
  ): Promise<AxiosResponse<any, any>> {
    return client.request({
      method: 'get',
      url: `${config.api.openWeather}/data/2.5/onecall?`,
      params: {
        appid: config.appKey.openWeather,
        exclude: 'minutely,hourly,alerts',
        units: 'metric',
        lat: coordinates?.lat,
        lon: coordinates?.lng,
      },
    });
  },
};

export {OpenCageServices, OpenWeatherServices};
