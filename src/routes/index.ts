import {Dashboard} from 'modules';

const PublicRoutes = [
  {
    name: '',
    exact: true,
    path: '/',
  },
  {
    name: 'Dashboard',
    component: Dashboard,
    exact: true,
    path: '/dashboard',
  },
];

export {PublicRoutes};
