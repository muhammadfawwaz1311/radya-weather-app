//#region IMPORT
// Libraries
import React, {Suspense} from 'react';
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  withRouter,
  Switch,
  RouteProps,
} from 'react-router-dom';
// Data
import {PublicRoutes} from 'routes';
// Assets
import './App.css';
//#endregion

export const PublicRoute = (props: RouteProps): JSX.Element => {
  const {location, path} = props;
  if (location?.pathname === '/' || path === '*')
    return <Redirect to="/dashboard" />;

  return <Route {...props} />;
};

const AppStackRoute = withRouter(() => {
  return (
    <div>
      <div>
        <Suspense fallback={<div className="loading-page"></div>}>
          <Switch>
            {PublicRoutes.map((route) => (
              <PublicRoute key={route.name} {...route} />
            ))}
          </Switch>
        </Suspense>
      </div>
    </div>
  );
});

const App: React.FC = () => {
  return (
    <Router>
      <AppStackRoute />
    </Router>
  );
};

export default App;
