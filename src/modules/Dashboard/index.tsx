//#region IMPORT
// Libraries
import React, {useState, useEffect} from 'react';
// Components
import {Footer, Header, Loader, Warning, WeatherAndForecast} from 'components';
// Data
import {OpenCageServices, OpenWeatherServices} from 'services';
// Assets
import './Dashboard.scss';
import {Coordinates} from 'services/entity';
//#endregion

const Dashboard: React.FC = () => {
  const [address, setAddress] = useState<string>('');
  const [coordinates, setCoordinates] = useState<Partial<Coordinates>>({});
  const [weatherAndForecastInfo, setWeatherAndForecastInfo] = useState({});
  const [locationInfo, setLocationInfo] = useState({});
  const [contentState, setContentState] = useState<string>('blank');

  const searchCity = (city: string) => {
    setAddress(city);
  };

  const showWarning = () => {
    setContentState('warning');
    setTimeout(() => setContentState('blank'), 3000);
  };

  useEffect(() => {
    const makeRequest = (position: any) => {
      setContentState('loading');
      OpenCageServices.getAddressOfCoordinates(
        position.coords.latitude,
        position.coords.longitude,
      )
        .then((res) => {
          const locationData = res.data.results[0].components;
          setLocationInfo({
            village: locationData.village,
            city: locationData.city,
            town: locationData.town,
            state: locationData.state,
            country: locationData.country,
          });
        })
        .then(() =>
          setCoordinates({
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          }),
        )
        .catch(() => showWarning());
    };

    const catchError = (err: any) => {
      alert('ERROR(' + err.code + '): ' + err.message);
    };

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(makeRequest, catchError);
    } else {
      alert('Geolocation is not supported by this browser.');
    }
  }, []);

  useEffect(() => {
    if (address === '') return;

    setContentState('loading');
    OpenCageServices.getCoordinatesOfAddress(address)
      .then((res) => {
        if (
          res.data.results.length === 0 ||
          (res.data.results[0].components.city === undefined &&
            res.data.results[0].components.town === undefined)
        ) {
          showWarning();
          return;
        }

        setCoordinates(res.data.results[0].geometry);
        const locationData = res.data.results[0].components;
        setLocationInfo({
          village: locationData.village,
          city: locationData.city,
          town: locationData.town,
          state: locationData.state,
          country: locationData.country,
        });
      })
      .catch(() => showWarning());
  }, [address]);

  useEffect(() => {
    if (Object.keys(coordinates).length === 0) return;

    OpenWeatherServices.getWeatherAndForecast(coordinates)
      .then((res) => {
        setWeatherAndForecastInfo(res.data);
        setContentState('weatherAndForecast');
      })
      .catch(() => showWarning());
  }, [coordinates]);

  const DashboardContent: any = {
    blank: () => null,
    loading: () => <Loader />,
    warning: () => <Warning />,
    weatherAndForecast: () => (
      <WeatherAndForecast
        weatherInfo={weatherAndForecastInfo}
        location={locationInfo}
      />
    ),
  };

  return (
    <div className="App">
      <div className="App__container">
        <div>
          <Header searchCity={searchCity} />
          {DashboardContent[contentState]()}
        </div>
        <Footer />
      </div>
    </div>
  );
};

export default Dashboard;
