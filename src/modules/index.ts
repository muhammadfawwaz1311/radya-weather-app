import {lazy} from 'react';

const Dashboard = lazy(() => import('modules/Dashboard'));

export {Dashboard};
