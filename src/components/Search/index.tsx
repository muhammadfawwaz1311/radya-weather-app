//#region IMPORT
// Libraries
import React, {ChangeEvent, KeyboardEvent, useState} from 'react';
// Assets
import './Search.scss';
//#endregion

interface Props {
  searchCity(city: string): void;
}

const Search: React.FC<Props> = ({searchCity}) => {
  const [currentCity, setCurrentCity] = useState<string>('');

  function handleInputChange(event: ChangeEvent<HTMLInputElement>) {
    setCurrentCity(event.target.value);
  }

  function handleButtonClick() {
    if (currentCity.trim() === '') return;
    searchCity(currentCity);
  }

  function handleKeyPress(event: KeyboardEvent<HTMLInputElement>) {
    if (event.key === 'Enter') handleButtonClick();
  }

  return (
    <div className="Search">
      <label className="Search__label">
        <input
          className="Search__input"
          value={currentCity}
          onChange={handleInputChange}
          onKeyPress={handleKeyPress}
        />
      </label>
      <button className="Search__button" onClick={handleButtonClick}>
        Search
      </button>
    </div>
  );
};

export default Search;
