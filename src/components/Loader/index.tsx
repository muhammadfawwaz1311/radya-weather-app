//#region IMPORT
// Libraries
import React from 'react';
// Assets
import './Loader.scss';
//#endregion

const Loader: React.FC = () => {
  return <div className="Loader"></div>;
};

export default Loader;
