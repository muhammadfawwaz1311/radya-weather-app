//#region IMPORT
// Libraries
import React from 'react';
// Assets
import './Footer.scss';
//#endregion

const Footer: React.FC = () => {
  return (
    <footer className="Footer">
      <p>
        Created by &nbsp;
        <a
          href="https://gitlab.com/muhammadfawwaz1311/radya-weather-app"
          target="_blank"
          rel="noreferrer">
          Muhammad Fawwaz Izzuddin
        </a>
      </p>
    </footer>
  );
};

export default Footer;
