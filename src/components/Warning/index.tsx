//#region IMPORT
// Libraries
import React from 'react';
// Assets
import './Warning.scss';
//#endregion

const Warning: React.FC = () => {
  return (
    <div className="Warning">
      <h2> No location found </h2>
      <p>Try informing city/town and state/country</p>
      <p> Ex: Bandung, Indonesia</p>
      <p> Ex: Jakarta, Indonesia </p>
    </div>
  );
};

export default Warning;
