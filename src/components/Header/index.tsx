//#region IMPORT
// Libraries
import React from 'react';
// Components
import {Search} from 'components';
// Assets
import './Header.scss';
//#endregion

interface Props {
  searchCity(city: string): void;
}

const Header: React.FC<Props> = ({searchCity}) => {
  return (
    <header className="Header">
      <h1 className="Header__title">Radya&apos;s Weather</h1>
      <Search searchCity={searchCity} />
    </header>
  );
};

export default Header;
