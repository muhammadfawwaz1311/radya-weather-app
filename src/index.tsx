//#region IMPORT
// Libraries
import React, {StrictMode} from 'react';
import ReactDOM from 'react-dom';
// Components
import App from './App';
//#endregion

const rootElement = document.getElementById('root');
ReactDOM.render(
  <StrictMode>
    <App />
  </StrictMode>,
  rootElement,
);
