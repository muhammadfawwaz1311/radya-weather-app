interface EnvConfig {
  api: any;
  appKey: any;
}

const config: EnvConfig = {
  api: {
    openCage: 'https://api.opencagedata.com',
    openWeather: 'https://api.openweathermap.org',
  },
  appKey: {
    openCage: '103fda4edfee402fa23d1d1e4d742132',
    openWeather: '1b825daf58d455d1161c0dd1607a1aec',
  },
};
export default config;
